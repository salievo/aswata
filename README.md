# API Moringa - Aswata

### 01 Check Referral ID
##### - Features
  - Sender : **ASWATA**
  - Target : **MORINGA**
  - Check Referral ID
#

##### - Endpoint
```sh
http://api.moringaku.com:8280/partner/aswata/checkref
```
##### - Method : POST

##### - Authorization Basic Auth
| Params | Data Type | Mandatory | 
|--|--|--|
|username| [text] | Y |
|password|[text] | Y |

#
##### - Body Structure

| Params |  Data Type | Mandatory |  Description |
|--|--|--|--|
| ReferenceCode | [text] | Y | Referral to check |

#

##### - Result Structure
| Params | Data Type | Mandatory | Length | 
|--|--|--|--|
| status | [number] | Y | 4 |
| message | [text] | Y | 100 |
| data | [jsonobject] |  |  |

#

##### - Sample Call:
###### JQuery Ajax Call 
```sh
$.ajax({
    url: "http://api.moringaku.com:8280/partner/aswata/checkref",
    authorization: {
        "type": "Basic Auth",
        "username": "{username}",
        "password":"{password}"
    }
    
    dataType: "json",
    type : "POST",
    data: 
    {
        "ReferenceCode" : "9895df1ba413231f"
    
    }
  });
```

#

##### Sample response:
###### Valid ReferralID
```sh
{
    "status": 200,
    "message": "0",
    "data" : {}
}
```
###### Not Valid ReferralID
```sh
{
    "status": 200,
    "message": "1",
    "data" : {}
}
```

#

# ___________________________________________________________

### 02 Submit aplication Data
##### - Features
  - Sender : **ASWATA**
  - Target : **MORINGA**
  - Submit data aplication. Triggered after registration and survey
#

##### - Endpoint
```sh
http://api.moringaku.com/partner/aswata/submission
```
##### - Method : POST

##### - Authorization Basic Auth
| Params | Data Type | Mandatory | 
|--|--|--|
|username| [text] | Y |
|password|[text] | Y |

#

##### - Body Structure

| Params | Detail |Data Type | Mandatory 
|--|--|--|--|--|
| ProductName           || [text] | |
| PackageName           || [text] | |
| ProductType           || [number] |
| Status                || [text] | REGISTRATION/SURVEYDONE |
| Premium               || [number] |
| SumInsured            || [number] |
| ReferenceCode         || [text] | |
| TransactionDate       || [text] | |
| LinkPayment           || [text] | Filled when SURVEYDONE |
| PolicyHolder          || [jsonobject] | |
|| FullName             | [text] ||
|| Email                | [text] ||
|| Phone                | [text] ||
|| KTPNo                | [text] ||
|| DOB                  | [text] ||
|| Address              | [text] ||
|| Sex                  | [text] ||
|| ProvinceName         | [text] ||
|| CityName             | [text] ||
|| ZipCode              | [text] ||
|| CarType              | [text] ||
|| CarYear              | [text] ||
|| Color                | [text] ||
|| PoliceNumber         | [text] ||
|| BodyNumber           | [text] ||
|| MachineNumber        | [text] ||

#

##### - Result Structure
| Params | Data Type | Mandatory |  
|--|--|--|
| status | [number] | Y |
| message | [text] | Y | 
| data | [jsonobject] |  |

#



##### - Sample Call:
###### JQuery Ajax Call 
```sh
$.ajax({
    url: "http://api.moringaku.com/partner/aswata/submission",
    authorization: {
        "type": "Basic Auth",
        "username": "{username}",
        "password":"{password}"
    }
    
    dataType: "json",
    type : "POST",
    data: {
            "ProductName":"Asuransi Oto A Plus",
            "PackageName":"Signature",
            "ProductType":"1",
            "Status":"SURVEYDONE",
            "Premium":1500000.0,
            "SumInsured":84000000.0,
            "ReferenceCode":"Ref01",
            "TransactionDate":"10/12/2019",
            "LinkPayment":"http://aswata.co.id/payment/busreqid=123131313",
            "PolicyHolder": {
                "FullName":"Jhone Doe",
                "Email":"jhone@outlook.com",
                "Phone":"085623421112",
                "KTPNo":"asdasd",
                "DOB":"01/08/1990",
                "Address":"asdasd",
                "Sex":"L",
                "ProvinceName":"Jawa Barat",
                "CityName":"Cirebon",
                "ZipCode":"12345",
                "CarType":"Toyota",
                "CarYear":"2017",
                "Color":"Hitam",
                "PoliceNumber":"B777XIA",
                "BodyNumber":"MCG1238889117GCG",
                "MachineNumber":"FGGAS76177HG"
            }
    },
  });
```
#

##### Sample response:
###### Success Response
```sh
{
    "status": 200,
    "message": "Success",
    "data": {
    }
}
```
###### Failed Response
```sh
{
    "status": 500,
    "message": "There is an error in system",
    "data": {
    }
}
```
#
____________________________________________________________________

### 03 Submit Payment Status
##### - Features
  - Sender : **ASWATA**
  - Target : **MORINGA**
  - Submit payment status
#

##### - Endpoint
```sh
http://api.moringaku.com/partner/aswata/payment
```
##### - Method : POST

##### - Authorization Basic Auth
| Params | Data Type | Mandatory | 
|--|--|--|
|username| [text] | Y |
|password|[text] | Y |

#

##### - Body Structure
| Params | Data Type | Mandatory | 
|--|--|--|
|ReferenceCode|[text] | Y | 
|PolicyNo|[text] | Y | 
|PolicySoftCopyLink|[text] | Y | 
|Status| [number] | Y (0:GAGAL ; 1: BERHASIL) |

#

##### - Result Structure
| Params | Data Type | Mandatory | 
|--|--|--|
| status | [Text] | Y |
| message | [Text] | Y |
| data | [jsonObject]  |  | 

#

##### - Sample Call:
###### JQuery Ajax Call 
```sh
$.ajax({
    url: "http://api.moringaku.com/partner/aswata/payment",
    authorization: {
        "type": "Basic Auth",
        "username": "{username}",
        "password":"{password}"
    }
    
    dataType: "json",
    type : "POST",
    data: {
            "ReferenceCode":"Ref001",
            "PolicyNo":"8190100001",
            "PolicySoftCopyLink":"https://aswata.co.id/cetakpolis?polNo=8190100001",
            "Status": 1,
    }
  });
```
#

##### Sample response:
###### Success Response
```sh
{
    "status": 200,
    "message": "Success",
    "data": {
    }
}
```
###### Failed Response
```sh
{
    "status": 500,
    "message": "There is an error in system",
    "data": {
    }
}
```
#

____________________________________________________________________




### 04 Get Data By Date
##### - Features
  - Sender : **Moringa**
  - Target API : **Aswata**
  - Get All Transaction in 1 day
#

##### - Endpoint

```sh
https://api.aswata.co.id/api/eservices
```
#

##### - Method : POST

##### - Body Structure
| Params | |Data Type |Length/Format| Mandatory | Description
|--|--|--|--|--|--|
|service_code||[text]|30|Y|Code of the service to call: **MCSVC_GET_MV_ORDERS**|
|user_ref_no||[text]|30|Y|Generated unique reference number|
|client_code||[text]|10|Y|Code of the client API, put **MORINGAKU** as client_code|
|request_time||[text]|yyyymmddhhmiss|Y|Time stamp of the request Ex: **20170818203010**|
|auth_code||[text]||Y|Hash256(service_code+user_ref_no+request_time+client_code+key)
|request_param||[jsonObject]||||
||date|[text]|dd/MM/yyyy|Y|Date data to show|
#

##### - Result Structure

|Response Field||||Type|Length/Format|Nullable|Description|
|--|--|--|--|--|--|--|--|--|
|service_code||||Char|30(alphanumeric)|N|Code of the service to call:MCSVC_GET_MV_ORDERS
|user_ref_no||||Char|30(alphanumeric)|N|Generated reference number
|client_code||||char|10|N|Code of the client 
|response_time||||char|yyyymmddhhmiss|N|Time stamp of the response Ex: 20170818203010
|response_code||||char|6|N|Response code; see response code list
|error_desc||||char|200|Y|Error description if some error happen
|auth_code||||char||N|Hash256(service_code+ user_ref_no+response_time+client_code+key)
|response_param||||object|||JSON object child content
||data|||jsonArray|||
||| ProductName           || [text] | |
||| PackageName           || [text] | |
||| ProductType           || [number] | |
||| Premium               || [number] | |
||| SumInsured            || [number] | |
||| ReferenceCode         || [text] | | |
||| TransactionDate       || [text] | | |
||| PaymentStatus         || [number] | |
||| PaymentDate           || [text] | |
||| PolicyHolder          || [jsonobject] | |
|||| FullName             | [text] ||
|||| Email                | [text] ||
|||| Phone                | [text] ||
|||| KTPNo                | [text] ||
|||| DOB                  | [text] ||
|||| Address              | [text] ||
|||| Sex                  | [text] ||
|||| ProvinceName         | [text] ||
|||| CityName             | [text] ||
|||| ZipCode              | [text] ||
|||| CarType              | [text] ||
|||| CarYear              | [text] ||
|||| Color                | [text] ||
|||| PoliceNumber         | [text] ||
|||| BodyNumber           | [text] ||
|||| MachineNumber        | [text] ||

#

##### - Response Code List
|RESPONSE CODE| DESCRIPTION
|--|--|
|777777| Invalid service code
|888888 |Invalid token (auth code)
|999999| General error
|000000| Success
|000001| Format data error
|000002| Mandatory field are not fulfilled
#

##### - Sample Call:
###### JQuery Ajax Call 
```sh
$.ajax({
    url: "https://api.aswata.co.id/api/eservices",
    dataType: "json",
    type : "POST",
	data: {
		"service_code": "MCSVC_GET_MV_ORDERS",
		"user_ref_no": "903.2019.000062",
		"client_code": "MORINGAKU",
		"request_time": "20190716120730",
		"auth_code": "1380BA38B520C17C300BA00E248D8F9BF5DC9C725B5FB42FB1BA01F82783F097",
		"request_param": {
			"date": "17/12/2019"
		}
	}
  });
```
#

##### - Sample response:
###### Success Response
```sh
{
	"service_code": "MCSVC_GET_MV_ORDERS",
	"user_ref_no": "903.2019.000062",
	"client_code": "MORINGAKU",
	"response_time": "20190716120800",
	"response_code": "000000",
	"error_desc": "",
	"auth_code": "6CE138C1F4036E0C40C7D15C04696D32FEEFE17F5A8C3189E0513EE78EBD5BC4",
	"response_param": {
		"data": [{
			"ProductName": "Asuransi Oto A Plus",
			"PackageName": "Signature",
			"ProductType": "1",
			"Premium": 1500000.0,
			"SumInsured": 84000000.0,
			"ReferenceCode": "Ref01",
			"TransactionDate": "10/12/2019",
			"PaymentStatus": 1,
			"PaymentDate": "18/12/2019",
			"PolicyHolder": {
				"FullName": "Jhone Doe",
				"Email": "jhone@outlook.com",
				"Phone": "085623421112",
				"KTPNo": "asdasd",
				"DOB": "01/08/1990",
				"Address": "asdasd",
				"Sex": "L",
				"ProvinceName": "Jawa Barat",
				"CityName": "Cirebon",
				"ZipCode": "12345",
				"CarType": "Toyota",
				"CarYear": "2017",
				"Color": "Hitam",
				"PoliceNumber": "B777XIA",
				"BodyNumber": "MCG1238889117GCG",
				"MachineNumber": "FGGAS76177HG"
			}
		}]
	}
},
```
###### Failed Response
```sh
{
   "response_time": "20191219090851",
   "response_code": "888888",
   "error_desc": "Invalid token (auth code)"
}
```
#

____________________________________________________________________
